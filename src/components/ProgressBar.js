import React from "react";
import "./ProgressBar.css";
import { Grid } from "@mui/material";

const ProgressBar = () => {
	return (

		<Grid container direction="row" justifyContent="center" alignItems="flex-start" >
			<Grid item xs={1} className="stepper-icon">
				<img src="assets/Wizard-Step1.png" alt="step-1.png"></img>
				<h5 className="active">STEP 1: <br /> CREATE YOUR ACCOUNT PASSWORD</h5>
			</Grid>
			<Grid item xs={0.5} className="horizontal-bar">
				<img src="assets/Wizard-HorizontalBar.png" alt="horizontabar.png"></img>
			</Grid>
			<Grid item xs={1} className="stepper-icon">
				<img src="assets/Wizard-Step2.png" alt="step-2.png"></img>
				<h5>STEP 2: <br /> PERSONAL INFORMATION</h5>
			</Grid>
			<Grid item xs={0.5} className="horizontal-bar">
				<img src="assets/Wizard-HorizontalBar.png" alt="horizontabar.png"></img>
			</Grid>
			<Grid item xs={1} className="stepper-icon">
				<img src="assets/Wizard-Step3.png" alt="step-3.png"></img>
				<h5>STEP 3: <br /> EMPLOYMENT DETAILS</h5>
			</Grid>
			<Grid item xs={0.5} className="horizontal-bar">
				<img src="assets/Wizard-HorizontalBar.png" alt="horizontabar.png"></img>
			</Grid>
			<Grid item xs={1} className="stepper-icon">
				<img src="assets/Wizard-Step4.png" alt="step-4.png"></img>
				<h5>STEP 4: <br /> UPLOAD DOCUMENTS</h5>
			</Grid>
			<Grid item xs={0.5} className="horizontal-bar">
				<img src="assets/Wizard-HorizontalBar.png" alt="horizontabar.png"></img>
			</Grid>
			<Grid item xs={1} className="stepper-icon">
				<img src="assets/Wizard-Step5.png" alt="step-5.png"></img>
				<h5>STEP 5: <br /> COMPLETE</h5>
			</Grid>
		</Grid>

	)
}

export default ProgressBar
