import Box from "@mui/material/Box";
import { Typography } from "@mui/material"
import React from "react";
import "./Header.css";
import ProgressBar from "./ProgressBar";


const Header = () => {
	return (
		<div>
			<div className="header">
				<div className="header-title">
					<img src="assets/Logo.png"></img>
				</div>
			</div>
			<ProgressBar />
		</div>
	);
}

export default Header;
