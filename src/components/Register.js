import { Button, CircularProgress, TextField, Grid, Avatar } from "@mui/material";
import { Box } from "@mui/system";
import axios from "axios";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import { config } from "../App";
import Header from "./Header";
import "./Register.css";


const Register = () => {
	const { enqueueSnackbar } = useSnackbar();
	const [isLoading, setIsLoading] = useState(false)
	const [formData, setFormData] = useState({
		username: "",
		email: "",
		image: "",
		password: "",
		confirmPassword: ""
	})

	const onChangeHandler = (e) => {
		const name = e.target.name;
		if (name === "image") {
			const value = e.target.files[0];
			setFormData({ ...formData, [name]: value })
		} else {
			const value = e.target.value;
			setFormData({ ...formData, [name]: value })
		}

	}

	const register = async (formData) => {

		if (!validateInput(formData)) return;

		setIsLoading(true);
		try {
			const body = { name: formData.username, email: formData.email, password: formData.password, image: formData.image }
			const url = "http://localhost:8082/v1/register";
			const response = await axios({
				method: "post",
				url: url,
				data: body,
				headers: { "Content-Type": "multipart/form-data" },
			});
			console.log(response)
			setFormData({
				username: "",
				email: "",
				image: "",
				password: "",
				confirmPassword: ""
			})
			setIsLoading(false);
			enqueueSnackbar("Registered successfully", { variant: "success" });

		}
		catch (e) {
			setIsLoading(false);
			console.log(e)
			if (e.response && e.response.status === 400) {
				return enqueueSnackbar(e.response.data.message, { variant: "error" })
			}
			else {
				return enqueueSnackbar("Something went wrong", { variant: "error" })
			}
		}


	};


	const validateInput = (data) => {
		if (!data.username) {
			enqueueSnackbar("Username is a required field", { variant: "warning" })
			return false;
		}
		if (data.username.length < 6) {
			enqueueSnackbar("Username must be at least 6 characters", { variant: "warning" })
			return false;
		}
		if (!data.password) {
			enqueueSnackbar("Password is a required field", { variant: "warning" })
			return false;
		}
		if (data.password.length < 6) {
			enqueueSnackbar("Password must be at least 6 characters", { variant: "warning" })
			return false;
		}
		if (data.password !== data.confirmPassword) {
			enqueueSnackbar("Passwords do not match", { variant: "warning" })
			return false;
		}
		if (data.image === null) {
			enqueueSnackbar("Please select an Image", { variant: "warning" });
			return false;
		}
		return true
	};

	return (
		<Box
			display="flex"
			flexDirection="column"
			justifyContent="center"
			minHeight="10vh">
			<Header />
			<div>
				<div className="page-name">
					<h3> CREATE YOUR ACCOUNT</h3>
				</div>
				<div className="page-content">
					<p > Because there will be document that you need to apply for the loan to get start off by creating password so that you cna
						login to you account once you that you can login to your account once you have these document ready.</p>
				</div>

			</div>
			<Box
				display="flex"
				flexDirection="row"
				justifyContent="center"
				alignItems="center"
				minHeight="10vh">
				<Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
					<Grid item xs={2} height="10vh">
						<Grid container align="center" >
							<Grid item xs={12} >
								<Avatar
									alt="avatar-img"
									src="assets/Avatar.png"
									sx={{ width: 100, height: 100 }}
								/>
								<input type="file"
									id="image"
									name="image"
									onChange={onChangeHandler}
									hidden />
								<button id="custom-button" onClick={() => {
									const imageBtn = document.getElementById("image");
									imageBtn.click();
								}}> Upload</button>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={10}>
						<Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
							<Grid item xs={5}>
								<TextField
									id="username"
									label="Name"
									variant="outlined"
									title="Username"
									name="username"
									placeholder="Enter Username"
									value={formData.username}
									onChange={onChangeHandler}
									fullWidth
								/>
							</Grid>
							<Grid item xs={5}>
								<TextField
									id="email"
									label="email"
									variant="outlined"
									title="Email"
									name="email"
									placeholder="Enter Email"
									value={formData.email}
									onChange={onChangeHandler}
									fullWidth
								/>
							</Grid>
							<Grid item xs={5}>
								<TextField
									id="password"
									variant="outlined"
									label="Password"
									name="password"
									type="password"
									value={formData.password}
									onChange={onChangeHandler}
									fullWidth
									placeholder="Enter a password with minimum 6 characters"
								/>
							</Grid>
							<Grid item xs={5}>
								<TextField
									id="confirmPassword"
									variant="outlined"
									label="Confirm Password"
									name="confirmPassword"
									type="password"
									value={formData.confirmPassword}
									onChange={onChangeHandler}
									fullWidth
								/>
							</Grid>
						</Grid>

					</Grid>
				</Grid>
			</Box>
			{(isLoading) ? <Box display="flex" justifyContent="center" alignItems="center">
				<CircularProgress size="25" color="primary" />
			</Box> : <Button id="button-sub" variant="contained" onClick={async () => {
				console.log("button clicked")
				await register(formData)
			}}>
				Save & Next
			</Button>}
		</Box>
	);
};

export default Register;
