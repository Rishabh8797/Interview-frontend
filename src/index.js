import React from "react";

import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom"
import { SnackbarProvider } from "notistack";
import { createRoot } from "react-dom/client"


const root = createRoot(document.getElementById('root'))
root.render(<React.StrictMode>
  <BrowserRouter>
    <SnackbarProvider
      maxSnack={1}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      preventDuplicate>
      <App />
    </SnackbarProvider>
  </BrowserRouter>
</React.StrictMode>)


