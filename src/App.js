import Register from "./components/Register";


export const config = {
  endpoint: `http://localhost:8082/v1`,
};


function App() {
  return (
    <div className="App">
      <Register />
    </div>
  );
}

export default App;

